define('lib/ui/class',[],function() {
	var Class = (function() {
		
		function subclass() {}

		function isFunction(val) {
			return Object.prototype.toString.call(val) == '[object Function]';
		}

		function ext() {
			var arg = arguments,
				a = arg.length == 1 ? NM : arg[0],
				b = arg.length > 1 ? arg[1] : arg[0];
			if (b == null) return a;
			try {
				for (var n in b) {
					!a.hasOwnProperty(b[n]) && (a[n] = b[n] );
				}
				return a;
			} catch (e) {

			}
		}
		var extend = function () {
	        var arg = arguments,
	            a = arg.length == 1 ? NM : arg[0],
	            b = arg.length > 1 ? arg[1] : arg[0];
	        if (b == null) return a;
	        try {
	            for (var n in b) { !a.hasOwnProperty(b[n]) && ((typeof a == 'object' && (a[n] = b[n])) || (typeof a == 'function' && (a.prototype[n] = b[n]))); }
	            return a;
	        } catch (e) {
	
	        }
	    };
		function create() {
			var parent = null,
				properties = arguments;
			if (isFunction(properties[0]))
				parent = properties.shift();

			function klass() {
				this.init.apply(this, arguments);
			}

			ext(klass, Class.Methods);
			extend(klass,{
				extend:extend,
				setOptions:setOptions
			});
			klass.superclass = parent;
			klass.subclasses = [];

			if (parent) {
				subclass.prototype = parent.prototype;
				klass.prototype = new subclass;
				parent.subclasses.push(klass);
			}

			for (var i = 0, length = properties.length; i < length; i++)
				klass.addMethods(properties[i]);

			if (!klass.prototype.init)
				klass.prototype.init = function(){};

			klass.prototype.constructor = klass;
			return klass;
		}
		
		function setOptions(newOpt){
			this.extend(this.options,newOpt);
			this.extend(this,this.options);
		}
		
		function addMethods(source) {
			var _hasOwnProperty = Object.prototype.hasOwnProperty,
				properties = [];

			for (var property in source) {
				if (_hasOwnProperty.call(source, property)) {
					properties.push(property);
				}

			}

			for (var i = 0, length = properties.length; i < length; i++) {
				var property = properties[i],
					value = source[property];
				this.prototype[property] = value;
			}

			return this;
		}
		return {
			create: create,
			Methods: {
				addMethods: addMethods
			}
		};
	})();
	return Class;
});
define(
    'service/index619/comm',['jQuery', 'lib/ui/class'],
    function ($, Class) {
        var comm = Class.create({
            setOption: function (opts) {
                var options = {}
                $.extend(true, options, opts);
            }
        }, {
            init: function (opts) {
                this.setOption(opts);
                this.bindEvent();
                this.TagHover(".tip-show-btn", ".tip-show", 35, 70);
                this.initFuction();
                this.MenuWidth();
                this.sTab(".nav_cart", ".nav_cart_sub");//�������ﳵtab
                this.TimeNav(".nav_sort_a", ".nav_sort_sub_a", ".navtaghd_a", ".navtagbd_a");
                this.TimeNav(".nav_sort_b", ".nav_sort_sub_b", ".navtaghd_b", ".navtagbd_b");
                this.NavTagHover(".navtaghd_a", ".navtagbd_a");
                this.NavTagHover(".navtaghd_b", ".navtagbd_b");
                this.scrollEvent();
                this.DivHeight(".sitebar-out", 0);
                this.DivHeight(".sitebar", 0);
                this.DivHeight(".sitebar-box", 0);
                this.DivHeight(".sitebar-con-bd", 35);
            },
            scrollEvent: function () {
                var nav_top = $(".nav"), ah = 0;
                var nav_topH = $(".nav").height();
                var haederH = $(".header").height();

                function scrollMenu(){
                    var yy = $(this).scrollTop();
                    if ($(".banner-spread").css("display") == "block") {
                        ah = $(".banner-spread").height()
                    } else {
                        ah = 0
                    }
                    var minY = haederH + ah + 46;
                    if ($(".detail_tit").length <= 0) {
                        if (yy >= minY) {
                            $(".nav-box").slideDown();
                        } else {
                            $(".nav-box").slideUp();
                        }
                    }
                    if ($(".left-bar").length > 0) {
                        if ((yy >= minY)) {
                            $(".left-bar").show()
                        } else {
                            $(".left-bar").hide()
                        }
                    }
                    if ($(".right-bar").length > 0) {
                        if ((yy >= minY)) {
                            $(".right-bar").show()
                        } else {
                            $(".right-bar").hide()
                        }
                    }
                }
                $(window).scroll(function () {
                    var topScroll=$(this).scrollTop();
                    if (navigator.userAgent.indexOf("MSIE 6.0") > 0 || navigator.userAgent.indexOf("MSIE 7.0") > 0) {
                          setTimeout(function(){
                              var scrollTop=$(window).scrollTop();
                              if(topScroll==scrollTop){
                                  scrollMenu();
                              }
                          },1000);
                    }else{
                        scrollMenu();
                    }

                })
            },
            TimeNav: function (onhover, hoverbox, taghd, tagbd) {
                var sTime;
                $(onhover).hover(
                    function () {
                        clearTimeout(sTime);
                        sTime = setTimeout(function () {
                            $(hoverbox).show();
                        }, 100);
                    },
                    function () {
                        clearTimeout(sTime);
                        sTime = setTimeout(function () {
                            $(hoverbox).hide();
                            $(taghd).removeClass("cur");
                            $(tagbd).addClass("hide");
                        }, 300);
                    }
                );
            },
            NavTagHover: function (taghdH, tagbdH) {
                var k = $(taghdH).length;
                for (var i = 0; i < k; i++) {
                    $(taghdH).eq(i).attr("rel", i)
                }

                $(taghdH).hover(function () {
                    var rel = $(this).attr("rel");
                    $(this).addClass("cur");
                    $(this).siblings().removeClass("cur");
                    $(tagbdH).eq(rel).removeClass("hide");
                    $(tagbdH).eq(rel).siblings(tagbdH).addClass("hide");
                })

            },
            tabs: function (tabId, tabNum) {
                $(tabId + " .tab a").removeClass("cur");
                $(tabId + " .tab a").eq(tabNum).addClass("cur");
                $(tabId + " .tabcon").hide();
                $(tabId + " .tabcon").eq(tabNum).show();
            },
            initFuction: function () {
                var _this = this;

                function CloseDiv(Closebtnn, Closeboxx) {
                    $(Closebtnn).live("click", function () {
                        $(Closeboxx).css({
                            "display": "none"
                        })
                    })
                }

                function DivMargin(DivMarginclass) {
                    if ($(DivMarginclass).length > 0) {
                        $(DivMarginclass).eq(0).css({
                            "margin-top": $(window).height() * 0.18 + "px"
                        });
                        $(window).resize(function () {
                            $(DivMarginclass).eq(0).css({
                                "margin-top": $(window).height() * 0.18 + "px"
                            })
                        })
                    }
                }

                DivMargin(".sitebar-tab01");

                function CloseBtn(closebtn, sitebarbox, sitebarall) {
                    $(closebtn).live("click", function () {
                        var boxwidth = $(this).parents(sitebarbox).width();
                        $(sitebarall).stop(false, false);
                        $(sitebarall).animate({
                            right: -boxwidth + "px"
                        }, 300, function () {
                            $(sitebarbox).css({
                                "visibility": "hidden",
                                "z-index": "999997"
                            });
                            _this.MenuWidth();
                        })
                    })
                }

                CloseBtn(".sitebar-box-close", ".sitebar-box", ".sitebar");

                function GotoTop(topbtn) {
                    function scrollT(){
                        if ($(window).scrollTop() > 0) {
                            $(topbtn).show(500)
                        } else {
                            $(topbtn).hide(500)
                        }
                    }
                    if ($(topbtn).length > 0) {
                        $(window).scroll(function () {
                            var topScroll=$(this).scrollTop();
                            if (navigator.userAgent.indexOf("MSIE 6.0") > 0 || navigator.userAgent.indexOf("MSIE 7.0") > 0) {
                                setTimeout(function(){
                                    var scrollTop=$(window).scrollTop();
                                    if(topScroll==scrollTop){
                                        scrollT();
                                    }
                                },1000);
                            }else{
                                scrollT();
                            }

                        });
                        $(topbtn).on("click", function () {
                            $("body,html").animate({
                                scrollTop: 0
                            }, 500)
                        })
                    }
                }

                GotoTop(".sitebar-tab09");

                function SpaceClick(sitebarallclass, sitebarall, sitebarbox, sitebarnumb) {
                    if ($(sitebarall).length > 0) {
                        $(document).live("click", function (e) {
                            var target = $(e.target);
                            if (target.hasClass(sitebarallclass) || target.parents().hasClass(sitebarallclass)) {
                                return
                            } else {
                                if ($(sitebarall).css("right") == "0px") {
                                    var sitebarwid = $(sitebarall).width();
                                    var siteconwid = $(sitebarall).width() - sitebarnumb;
                                    $(sitebarall).animate({
                                        right: -siteconwid + "px"
                                    }, 300, function () {
                                        $(sitebarbox).css({
                                            "visibility": "hidden",
                                            "z-index": "999997"
                                        });
                                        _this.MenuWidth();
                                    })
                                }
                            }
                        })
                    }
                }

                function SiteBar(sitebarbtn, sitebarcon, sitebarnumb, sitebarall) {
                    var sitebarallclass = $(sitebarall).attr("class");
                    var siteconwid = $(sitebarcon).width();
                    var siteallwid = siteconwid + sitebarnumb;
                    $(sitebarall).width(siteallwid + "px");
                    if ($(sitebarcon).css("visibility") == "hidden") {
                        $(sitebarcon).css({
                            "visibility": "visible",
                            "z-index": "999998"
                        });
                        $(sitebarall).stop(false, false);
                        $(sitebarall).animate({
                            right: "0"
                        }, 300)
                    } else {
                        $(sitebarall).stop(false, false);
                        $(sitebarall).animate({
                            right: -siteconwid + "px"
                        }, 300, function () {
                            $(sitebarcon).css({
                                "visibility": "hidden",
                                "z-index": "999997"
                            });
                            _this.MenuWidth();
                        })
                    }
                }

                function SiteBarShow(sitebarbtn, sitebarcon, sitebarnumb, sitebarall, sitebarbox) {
                    var siteconwid = $(sitebarcon).width();
                    var siteallwid = siteconwid + sitebarnumb;
                    $(sitebarall).width(siteallwid + "px");
                    if ($(sitebarcon).css("visibility") == "hidden") {
                        var siteconheig = $(sitebarbox).height();
                        $(sitebarbox).css({
                            "visibility": "hidden",
                            "z-index": "999997"
                        });
                        $(sitebarcon).css({
                            "visibility": "visible",
                            "z-index": "999998",
                            "top": siteconheig + "px"
                        });
                        $(sitebarcon).stop(false, false);
                        $(sitebarcon).animate({
                            top: "0"
                        }, 300)
                    } else {
                        $(sitebarall).stop(false, false);
                        $(sitebarall).animate({
                            right: -siteconwid + "px"
                        }, 300, function () {
                            $(sitebarcon).css({
                                "visibility": "hidden",
                                "z-index": "999997"
                            });
                            _this.MenuWidth();
                        })
                    }
                }

                function Sitebaranimate(sitebarbtn, sitebarcon, sitebarnumb, sitebarall, sitebarbox) {
                    $(sitebarbtn).live("click", function () {

                        var siteconwid = -($(sitebarall).width() - sitebarnumb);
                        if ($(sitebarall).css("right") == "0px") {
                            SiteBarShow(sitebarbtn, sitebarcon, sitebarnumb, sitebarall, sitebarbox)
                        }
                        if ($(sitebarall).css("right") == siteconwid + "px") {
                            SiteBar(sitebarbtn, sitebarcon, sitebarnumb, sitebarall)
                        }

                    })
                }


                function UnLogin(sitebarbtn, sitebarlogbox, sitebarlogclass, sitebarallclass, closebtn) {
                    if ($(sitebarlogbox).length > 0) {
                        $(sitebarbtn).live("click", function () {
                            var windowsH = $(window).height();
                            var sitebaralln = "." + sitebarallclass;
                            if ($(sitebaralln).attr("class").indexOf("site-min-height") > 0) {
                                var sitebarallnH = $(sitebaralln).height();
                                if (windowsH < sitebarallnH) {
                                    windowsH = sitebarallnH
                                }
                            }
                            var sitebarbtntop = $(this).offset().top - $(sitebaralln).offset().top - 1;
                            var bottomH = windowsH - sitebarbtntop;
                            var sitebarlogheight = $(sitebarlogbox).height();
                            var siteH = $(this).height();
                            if (bottomH > sitebarlogheight) {
                                $(sitebarlogbox).css({
                                    "display": "block",
                                    "top": sitebarbtntop + "px",
                                    "bottom": "auto"
                                });
                                $(sitebarlogbox).removeClass("curbottom")
                            } else {
                                $(sitebarlogbox).css({
                                    "display": "block",
                                    "top": "auto",
                                    "bottom": bottomH - siteH + "px"
                                });
                                $(sitebarlogbox).addClass("curbottom")
                            }
                            if ($(sitebarlogbox).css("display") == "block") {
                                $(document).live("mouseover", function (e) {
                                    var target = $(e.target);
                                    if (target.hasClass(sitebarallclass) || target.parents().hasClass(sitebarallclass) || target.hasClass(sitebarlogclass)) {
                                        return
                                    } else {
                                        $(sitebarlogbox).css({
                                            "display": "none"
                                        })
                                    }
                                })
                            }
                        });
                        $(closebtn).live("click", function () {
                            $(this).parents(sitebarlogbox).css({
                                "display": "none"
                            })
                        })
                    }
                }

                function SiteBarOne() {
                    SpaceClick("sitebar", ".sitebar", ".sitebar-box", 35);
                    Sitebaranimate(".sitebar-tab01", ".sitebar-con01", 35, ".sitebar", ".sitebar-box");
                    Sitebaranimate(".sitebar-tab02", ".sitebar-con02", 35, ".sitebar", ".sitebar-box")
                }

                function SiteBarOne_f() {
                    SpaceClick("sitebar", ".sitebar", ".sitebar-box", 35);
                    Sitebaranimate(".sitebar-tab02", ".sitebar-con02", 35, ".sitebar", ".sitebar-box");
                    CloseDiv(".sitebar-tab02", ".sitebar-login")
                }

                function SiteBarTwo() {
                    UnLogin(".sitebar-tab01", ".sitebar-login", "sitebar-login", "sitebar", ".sitebar-login-close")
                }

                function SiteBarsSH(judge) {
                    if (judge == 0) {
                        SiteBarTwo();
                        SiteBarOne_f()
                    }
                    if (judge == 1) {
                        SiteBarOne()
                    }
                };
                SiteBarsSH(1);
            },
            sTab: function (p, s) {
                var def;
                $(p).hover(
                    function () {
                        clearTimeout(def);
                        def = setTimeout(function () {
                            $(s).show()
                        }, 100);
                    },
                    function () {
                        clearTimeout(def);
                        def = setTimeout(function () {
                            $(s).hide()
                        }, 300);
                    }
                );
            },
            TagHover: function (taghdH, tagbdH, tagnumb01, tagnumb02) {
                $(taghdH).live("hover", function (event) {
                    if (event.type == "mouseenter") {
                        $(this).addClass("cur");
                        if ($(this).children(tagbdH).length > 0) {
                            $(this).children(tagbdH).stop(false, false);
                            $(this).children(tagbdH).css("display", "block");
                            $(this).children(tagbdH).animate({
                                right: tagnumb01 + "px",
                                opacity: "1"
                            }, 300)
                        }
                    } else {
                        $(this).removeClass("cur");
                        if ($(this).children(tagbdH).length > 0) {
                            $(this).children(tagbdH).stop(false, false);
                            $(this).children(tagbdH).animate({
                                right: tagnumb02 + "px",
                                opacity: "0"
                            }, 300, function () {
                                $(this).css("display", "none")
                            })
                        }
                    }
                })
            },
            MenuWidth: function () {
                if ($(window).width() <= 1235 && $(".sitebar").css("right") != "0px") {
                    var aa = $(".sitebar").width();
                    $(".sitebar").animate({
                        right: "-" + aa + "px"
                    }, 500);
                    $(".sitebar-fix").animate({
                        left: "-35px"
                    }, 500);
                    $(".sitebar-out").addClass("icon-hover");
                    $(".icon-hover").live("hover", function (event) {
                        if (event.type == "mouseenter") {
                            if (!$(".sitebar").is(":animated") && $(".sitebar").css("right") != "0px") {
                                var aa = $(".sitebar").width() - 35;
                                $(".sitebar").animate({
                                    right: "-" + aa + "px"
                                }, 500);
                                $(".sitebar-fix").animate({
                                    left: "0px"
                                }, 500)
                            }
                        } else {
                            if (!$(".sitebar").is(":animated") && $(".sitebar").css("right") != "0px") {
                                var aa = $(".sitebar").width();
                                $(".sitebar").animate({
                                    right: "-" + aa + "px"
                                }, 500);
                                $(".sitebar-fix").animate({
                                    left: "-35px"
                                }, 500)
                            }
                        }
                    })
                } else {
                    var aa = $(".sitebar").width() - 35;
                    $(".sitebar").animate({
                        right: "-" + aa + "px"
                    }, 500);
                    $(".sitebar-fix").animate({
                        left: "0px"
                    }, 500);
                    $(".sitebar-out").removeClass("icon-hover")
                }
            },
            DivHeight: function (DivClass, snumb) {
                if ($(DivClass).length > 0) {
                    $(DivClass).height($(window).height() - snumb);
                    $(window).resize(function () {
                        $(DivClass).height($(window).height() - snumb)
                    })
                }
            },
            bindEvent: function () {
                var _this = this;
                if ($(".banner-spread").length > 0 && $(".banner-spread").css("display") == "block") {
                    $(".banner-spread").slideDown();
                    $(".close-x").click(function () {
                        $(".banner-spread").slideUp();
                    })
                }

                $(window).resize(function () {
                    _this.MenuWidth();
                });
            }
        });
        return comm;
    });
define(
    'service/index619/zztHome',['jQuery', 'lib/ui/class'],
    function ($, Class) {
        var home = Class.create({
            setOptions: function (opts) {
                var options = {}
                $.extend(true, options, opts);
            }
        }, {
            init: function (opts) {
                this.setOptions(opts);
                this.initFunction();
                this.bindEvent();
            },
            initFunction: function () {
                var _this=this;
                setInterval(function(){
                    _this.showHotTime($('.time-left'));
                }, 1000);

                function ShowPre(o) {
                    var that = this;
                    this.box = $("." + o["box"]);
                    //this.box = $("#"+o.box);
                    this.btnP = $("." + o.Pre);
                    this.btnN = $("." + o.Next);
                    this.v = o.v || 1;
                    this.c = 0;
                    var li_node = "li";
                    this.loop = o.loop || false;

                    //ѭ������dom
                    if (this.loop) {
                        this.li = this.box.find(li_node);
                        this.box.append(this.li.eq(0).clone(true));
                    }
                    ;
                    this.li = this.box.find(li_node);
                    this.l = this.li.length;

                    //��������������
                    if (this.l <= this.v) {
                        this.btnP.hide();
                        this.btnN.hide();
                    }
                    ;
                    this.deInit = true;
                    this.w = this.li.outerWidth(true);
                    this.box.width(this.w * this.l);
                    this.maxL = this.l - this.v;

                    //Ҫ��ͼ���� ���¼�������
                    this.s = o.s || 1;
                    if (this.s > 1) {
                        this.w = this.v * this.w;
                        this.maxL = Math.floor(this.l / this.v);
                        this.box.width(this.w * (this.maxL + 1));
                        //������Ҫ��������
                        var addNum = (this.maxL + 1) * this.v - this.l;
                        var addHtml = "";
                        for (var adN = 0; adN < addNum; adN++) {
                            addHtml += '<li class="addBox"><div class="photo"></div><div class="text"></div></li>';
                        }
                        ;
                        this.box.append(addHtml);
                    }
                    ;

                    //����״̬ͼ��
                    this.numIco = null;
                    if (o.numIco) {
                        this.numIco = $("." + o.numIco);
                        var numHtml = "";
                        numL = this.loop ? (this.l - 1) : this.l;
                        for (var i = 0; i < numL; i++) {
                            numHtml += '<a href="javascript:void(0);">' + i + '</a>';
                        }
                        ;
                        this.numIco.html(numHtml);
                        this.numIcoLi = this.numIco.find("a");
                        this.numIcoLi.bind("click", function () {
                            if (that.c == $(this).html())return false;
                            that.c = $(this).html();
                            that.move();
                        });
                    }
                    ;
                    this.bigBox = null;
                    this.loadNumBox = null;
                    if (o.loadNumBox) {
                        this.loadNumBox = $("#" + o.loadNumBox);
                    }
                    ;

                    //��ǰ��������
                    this.allNumBox = null;
                    if (o.loadNumBox) {
                        this.allNumBox = $("#" + o.allNumBox);
                        if (o.bBox) {
                            var cAll = this.l < 10 ? ("0" + this.l) : this.l;
                        } else {
                            var cAll = this.maxL < 10 ? ("0" + (this.maxL + 1)) : (this.maxL + 1);
                        }
                        ;
                        this.allNumBox.html(cAll);
                    }
                    ;

                    //��ͼ��ť��������
                    if (o.bBox) {
                        this.bigBox = $("#" + o.bBox);
                        this.li.each(function (n) {
                            $(this).attr("num", n);
                            var cn = (n + 1 < 10) ? ("0" + (n + 1)) : n + 1;
                            $(this).find(".text").html(cn);
                        });
                        this.loadNum = 0;
                        this.li.bind("click", function () {
                            if (that.loadNum == $(this).attr("num"))return false;
                            var test = null;
                            if (that.loadNum > $(this).attr("num")) {
                                test = "pre";
                            }
                            ;
                            that.loadNum = $(this).attr("num");

                            that.loadImg(test);
                        });
                        that.loadImg();
                        if (o.bNext) {
                            that.bNext = $("#" + o.bNext);
                            that.bNext.bind("click", function () {
                                that.loadNum < that.l - 1 ? that.loadNum++ : that.loadNum = 0;
                                that.loadImg();
                            });
                        }
                        ;
                        if (o.bPre) {
                            that.bPre = $("#" + o.bPre);
                            that.bPre.bind("click", function () {
                                that.loadNum > 0 ? that.loadNum-- : that.loadNum = that.l - 1;
                                that.loadImg("pre");
                            });
                        }
                        ;
                    }
                    ;

                    //������������(ѭ��or��ѭ��)
                    if (this.loop) {
                        this.btnP.bind("click", function () {
                            if (that.c <= 0) {
                                that.c = that.l - 1;
                                that.box.css({left: -that.c * that.w});
                            }
                            ;
                            that.c--;
                            that.move(1);
                        });
                        this.btnN.bind("click", function () {
                            if (that.c >= (that.l - 1)) {
                                that.box.css({left: 0});
                                that.c = 0;
                            }
                            ;
                            that.c++;
                            that.move(1);
                        });
                    } else {
                        this.btnP.bind("click", function () {
                            that.c > 0 ? that.c-- : that.c = that.maxL;
                            that.move(1);
                        });
                        this.btnN.bind("click", function () {
                            that.c < that.maxL ? that.c++ : that.c = 0;
                            that.move(1);
                        });
                    }
                    ;
                    that.timer = null;
                    if (o.auto) {
                        that.box.bind("mouseover", function () {
                            clearInterval(that.timer);
                        });
                        that.box.bind("mouseleave", function () {
                            that.autoPlay();
                        });
                        that.autoPlay();

                    }
                    ;
                    this.move();
                }
                ShowPre.prototype = {
                    move: function (test) { //��������
                        var that = this;
                        var pos = this.c * this.w;
                        //document.title = (test&&that.timer);
                        if (test && that.timer) {
                            clearInterval(that.timer);
                        }
                        ;
                        //��ǰ����ͼ��
                        if (that.numIco) {
                            that.numIcoLi.removeClass("on");
                            var numC = that.c;
                            if (that.loop && (that.c == (this.l - 1))) {
                                numC = 0;
                            }
                            ;
                            that.numIcoLi.eq(numC).addClass("on");
                        }
                        ;

                        this.box.stop();
                        this.box.animate({left: -pos}, function () {
                            if (test && that.auto) {
                                that.autoPlay();
                            }
                            ;
                            if (that.loop && that.c == that.maxL) {
                                that.c = 0;
                                that.box.css({left: 0})
                            }
                            ;
                        });
                        if (that.bigBox)return false;
                        //���ô�ͼ��������
                        if (that.loadNumBox) {
                            var loadC = parseInt(that.c) + 1;
                            loadC = loadC < 10 ? "0" + loadC : loadC;
                            that.loadNumBox.html(loadC);
                        }
                        ;

                    },
                    autoPlay: function () { //�Զ����ŷ���
                        var that = this;
                        that.timer = setInterval(function () {
                            that.c < that.maxL ? that.c++ : that.c = 0;
                            that.move();
                        }, 5000);
                    }
                }
                this.DY_scroll(".brand-box1", ".more-arrow-up1", ".more-arrow-down1", ".brand-div1", 3, false);
                this.DY_scroll(".brand-box2", ".more-arrow-up2", ".more-arrow-down2", ".brand-div2", 3, false);
                this.TagClick(".tabbd", ".tab-content");

                if ($(".heros img").length > 1) {
                    var ShowPre1 = new ShowPre({
                        box: "heros",
                        Pre: "prev",
                        Next: "next",
                        numIco: "mask-left",
                        loop: 1,
                        auto: 1
                    });
                }
            },
            TagClick:function(taghd, tagbd) {
            var k = $(taghd).length;
            for (var i = 0; i < k; i++) {
                $(taghd).eq(i).attr("rel", i)
            }
            $(taghd).click(function () {
                var rel = $(this).attr("rel");
                $(this).addClass("cur");
                $(this).siblings().removeClass("cur");
                $(tagbd).eq(rel).removeClass("hide");
                $(tagbd).eq(rel).siblings(tagbd).addClass("hide")
            })
        },
            DY_scroll:function(wraper, prev, next, img, speed, or) {
            var wraper = $(wraper);
            var prev = $(prev);
            var next = $(next);
            //var img = $(img).find("ul");
            var img = $(img).find("ul>li");

            //var w = img.find("li").outerHeight(true);
            var w = img.find(".l-logo").outerHeight(true);
            var s = speed;
            next.click(function () {
                img.animate({
                    "margin-top": -w
                }, function () {
                    //img.find("li").eq(0).appendTo(img);
                    img.find(".l-logo").eq(0).appendTo(img);
                    img.find(".l-logo").eq(0).appendTo(img);
                    img.css({
                        "margin-top": 0
                    })
                })
            });
            prev.click(function () {
                //img.find("li:last").prependTo(img);
                img.find(".l-logo:last").prependTo(img);
                img.find(".l-logo:last").prependTo(img);
                img.css({
                    "margin-top": -w
                });
                img.animate({
                    "margin-top": 0
                })
            });
            if (or == true) {
                ad = setInterval(function () {
                    img.animate({
                        "margin-top": -w
                    }, function () {
                        img.find("li").eq(0).appendTo(img);
                        img.css({
                            "margin-top": 0
                        })
                    })
                }, s * 1000);
                wraper.hover(function () {
                    clearInterval(ad)
                }, function () {
                    ad = setInterval(function () {
                        next.click()
                    }, s * 1000)
                })
            }
        },
            showHotTime:function(TimeName) {
            var TimeLeft = TimeName;
            var NubTime = TimeLeft.length;
            var leftTime = new Array();
            var leftsecond = new Array();
            var date = new Array();
            var day1 = new Array();
            var hour = new Array();
            var minute = new Array();
            var second = new Array();
            var now = new Date();
            for (i = 0; i < NubTime; i++) {
                date[i] = new Date($(TimeLeft[i]).attr("data-time"));
                leftTime[i] = date[i].getTime() - now.getTime();
                leftsecond[i] = parseInt(leftTime[i] / 1000);
                if (leftsecond[i] > 0) {
                    day1[i] = Math.floor(leftsecond[i] / (60 * 60 * 24));
                    hour[i] = Math.floor((leftsecond[i] - day1[i] * 24 * 60 * 60) / 3600);
                    minute[i] = Math.floor((leftsecond[i] - day1[i] * 24 * 60 * 60 - hour[i] * 3600) / 60);
                    second[i] = Math.floor(leftsecond[i] - day1[i] * 24 * 60 * 60 - hour[i] * 3600 - minute[i] * 60);
                    if (day1[i] > 0) {
                        $(TimeLeft[i]).html("<b><em>����������</em>" + "<i>" + day1[i] + "</i>" + "��" + "<i>" + hour[i] + "</i>" + "ʱ" + "<i>" + minute[i] + "</i>" + "��" + "<i>" + second[i] + "</i>" + "��</b>")
                    } else {
                        if (hour[i] > 0) {
                            $(TimeLeft[i]).html("<b><em>����������</em>" + "<i>" + hour[i] + "</i>" + "ʱ" + "<i>" + minute[i] + "</i>" + "��" + "<i>" + second[i] + "</i>" + "��</b>")
                        } else {
                            if (minute[i] > 0) {
                                $(TimeLeft[i]).html("<b><em>����������</em>" + "<i>" + minute[i] + "</i>" + "��" + "<i>" + second[i] + "</i>" + "��</b>")
                            } else {
                                if (second[i] > 0) {
                                    $(TimeLeft[i]).html("<b><em>����������</em>" + "<i>" + second[i] + "</i>" + "��</b>")
                                } else {
                                    $(TimeLeft[i]).html("<b>��ѽ���</b>")
                                }
                            }
                        }
                    }
                } else {
                    $(TimeLeft[i]).html("<b>��ѽ���</b>")
                }
            }
        },
            bindEvent: function () {
                $("#h_banner").hover(
                    function () {
                        $("#index_b_hero .next").css({"display": "block"})
                        $("#index_b_hero .prev").css({"display": "block"})
                    },
                    function () {
                        $("#index_b_hero .next").css({"display": "none"})
                        $("#index_b_hero .prev").css({"display": "none"})
                    }
                );

                $(".l-logo").hover(
                    function () {
                        var c = $(".brand-logo").offset().top;
                        var d = $(".brand-logo").offset().left;
                        var a = $(this).offset().top - c - 9;
                        var b = $(this).offset().left - d - 319;
                        $(".logo-show").css({"top": a + "px", "left": b + "px"});
                        $(".logo-show").hide();
                        $(this).find(".logo-show").show();
                    }, function () {
                        $(".logo-show").hide();
                        $(".l-logo").css({"position": "", "z-index": ""});
                    }
                );
            }
        });
        return home;
    }
);
define(
    'service/index619/index',['jQuery', 'lib/ui/class', 'service/index619/comm', 'service/index619/zztHome'],
    function($,Class,Comm,Home){
        var index=Class.create({
            setOptions:function(opts){
                var options={

                }
                $.extend(true,options,opts);
            }
        },{
            init:function(opts){
                this.setOptions(opts);
                this.initFun();
            },
            initFun:function(){
                new Comm();
                new Home();
            }
        });
        return index;
    }
);
require(
    ['jQuery','service/index619/index','layout'],
    function($,Index){
        $(function(){
            new Index();
        });
    }
);
define("action/index", function(){});

/**
 * Created by leilihuang on 15/7/3.
 */
(function(){
    var obj = document.getElementById('requirejs'),
        baseJsUrl =obj&& obj.getAttribute('data-url')?obj.getAttribute('data-url') : '/',
        slot = baseJsUrl.match(/[a-zA-Z]\d/),
        isDebug = 0;
    //获取js路径


    if(slot && slot.length>0){
        slot =  slot[0];
        baseJsUrl = baseJsUrl.match(/http:\/\/[\w\.]*\//)[0];
    }

    function getBaseJsUrl(url){
        //return baseJsUrl+url;
        return "http://s.zzt.tm/portal-static/js/"+url;
    }

    var config = {
        paths: {
            jQuery: [
                getBaseJsUrl('jquery')
            ],
            layout: [
                getBaseJsUrl('jquery.lazyload')
            ]
        },
        shim: {
            jQuery: {
                deps: [],
                exports: '$'
            },
            layout: {
                deps: ['jQuery'],
                exports: 'Layout'
            }
        }
    };

    require.config(config);
})();
define("main", function(){});

