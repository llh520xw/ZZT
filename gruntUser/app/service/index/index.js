define(
    ['jQuery', 'lib/ui/class', 'service/index/comm', 'service/index/zztHome','layout'],
    function ($, Class, Comm,Home) {
        var index = Class.create({
                setOptions: function (opts) {
                    var options = {}
                    $.extend(true, options, opts);
                }
            }, {
                init: function (opts) {
                    this.setOptions(opts);
                    this.initFunction();
                },
                initFunction:function(){
                    new Comm();
                    new Home();
                }
            }
        );

        return index;
    }
)