define(
    ['jQuery', 'lib/ui/class'],
    function ($, Class) {
        var comm = Class.create({
            setOption: function (opts) {
                var options = {}
                $.extend(true, options, opts);
            }
        }, {
            init: function (opts) {
                this.setOption(opts);
                this.initFuction();
                this.bindEvent();
                this.TagHover(".taghd", ".tagbd");
                this.sTab(".nav_cart", ".nav_cart_sub");
            },
            initFuction: function () {
                var sTime;
                $(".nav_sort").hover(function () {
                    clearTimeout(sTime);
                    sTime = setTimeout(function () {
                        $(".nav_sort_sub").show();
                        $(".nav_sort i").addClass("down")
                    }, 100)
                }, function () {
                    clearTimeout(sTime);
                    sTime = setTimeout(function () {
                        $(".nav_sort_sub").hide();
                        $(".nav_sort i").removeClass("down");
                        $(".taghd").removeClass("cur");
                        $(".tagbd").addClass("hide")
                    }, 300)
                });

                /*               function tabs(tabId, tabNum) {
                 $(tabId + " .tab a").removeClass("cur");
                 $(tabId + " .tab a").eq(tabNum).addClass("cur");
                 $(tabId + " .tabcon").hide();
                 $(tabId + " .tabcon").eq(tabNum).show()
                 }
                 function set_foot(mm) {
                 var isIE = !! window.ActiveXObject;
                 var isIE6 = isIE && !window.XMLHttpRequest;
                 var Hfooter = $(".footer");
                 var footTop = Hfooter.offset().top;
                 var toppx = ($(window).height() / 2);
                 var Dtoppx = mm.outerHeight() / 2;
                 mm.css("top", toppx + "px");
                 mm.css("display", "none");
                 $(window).scroll(function() {
                 if ($(window).scrollTop() > 50) {
                 mm.css({
                 "display": "block"
                 })
                 } else {
                 mm.css({
                 "display": "none"
                 })
                 }
                 var maxYY = footTop - Dtoppx - toppx;
                 var yy = $(this).scrollTop();
                 if (!isIE6) {
                 if (yy < maxYY) {
                 mm.css({
                 "position": "fixed",
                 "top": toppx + "px",
                 "bottom": ""
                 })
                 } else {
                 mm.css({
                 "position": "fixed",
                 "top": toppx + "px",
                 "bottom": ""
                 })
                 }
                 }
                 })
                 };*/
            },
            sTab: function (p, s) {
                var def;
                $(p).hover(function () {
                    clearTimeout(def);
                    def = setTimeout(function () {
                        $(s).show()
                    }, 100)
                }, function () {
                    clearTimeout(def);
                    def = setTimeout(function () {
                        $(s).hide()
                    }, 300)
                })
            },
            TagHover: function (taghdH, tagbdH) {
                var k = $(taghdH).length;
                for (var i = 0; i < k; i++) {
                    $(taghdH).eq(i).attr("rel", i)
                }
                $(taghdH).hover(function () {
                    var rel = $(this).attr("rel");
                    $(this).addClass("cur");
                    $(this).siblings().removeClass("cur");
                    $(tagbdH).eq(rel).removeClass("hide");
                    $(tagbdH).eq(rel).siblings(tagbdH).addClass("hide")
                })
            },
            bindEvent: function () {
                $(".qzy-mt01").click(function () {
                    $(".qzy-mt01-img").toggle()
                });

                if ($(".banner-spread").length > 0) {
                    $(".banner-spread").slideDown();
                    $(".close-x").click(function () {
                        $(".banner-spread").slideUp()
                    })
                }
            }
        });
        return comm;
    });