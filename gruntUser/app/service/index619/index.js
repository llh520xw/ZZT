define(
    ['jQuery', 'lib/ui/class', 'service/index619/comm', 'service/index619/zztHome'],
    function($,Class,Comm,Home){
        var index=Class.create({
            setOptions:function(opts){
                var options={

                }
                $.extend(true,options,opts);
            }
        },{
            init:function(opts){
                this.setOptions(opts);
                this.initFun();
            },
            initFun:function(){
                new Comm();
                new Home();
            }
        });
        return index;
    }
)