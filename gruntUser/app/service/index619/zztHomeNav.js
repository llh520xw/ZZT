define(
    ['jQuery', 'lib/ui/class'],
    function ($, Class) {
        var nav = Class.create({
            setOptions: function (opts) {
                var options = {}
                $.extend(true, options, opts);
            }
        }, {
            init: function (opts) {
                this.setOptions(opts);
                /**新版去掉了右侧浮动菜单2015年7月27日11:38:50*/
                //this.initFunction();
                //this.bindEvent();
                //this.scroll();
            },
            initFunction: function () {

            },
            bindEvent: function () {
                $("#hnav_fi li").each(function () {
                    $(this).on('click', function () {
                        var abc = $(this).find("a").attr("href");
                        var scroll_offset = $(abc).offset();
                        $("body,html").animate({scrollTop: scroll_offset.top}, 500)
                    })
                });
                $("#H_go_top").on('click', function () {
                    scrollTo(0, 0)
                });
            },
            scroll: function () {
                var nav_top = $(".nav");
                var nav_topH = $(".nav").height();
                var haederH = $(".header").height(), ah = 0;

                var all_top = $("#H_side_icon"), _this = this;
                var Hfooter = $(".footer");
                var topLink = $(".hnav_fixed a");
                var one = $("#mrbk"),
                    two = $("#mrsx"),
                    three = $("#zarx"),
                    four = $("#jjsx"),
                    five = $("#zpp");

                function scrollEvent(scroH) {
                    var one_top = one.offset().top,
                        two_top = two.offset().top,
                        three_top = three.offset().top,
                        four_top = four.offset().top,
                        five_top = five.offset().top;

                    if (scroH <= one_top) {
                        _this.set_cur(topLink, topLink.eq(0));
                    } else if (scroH > one_top && scroH < three_top) {
                        _this.set_cur(topLink, topLink.eq(1));
                    } else if (scroH >= three_top && scroH < four_top) {
                        _this.set_cur(topLink, topLink.eq(2));
                    } else if (scroH >= four_top && scroH < five_top) {
                        _this.set_cur(topLink, topLink.eq(3));
                    } else {
                        _this.set_cur(topLink, topLink.eq(4));
                    }

                    var bb = 1106;
                    var top = all_top.offset().top;
                    var footTop = Hfooter.offset().top;
                    var maxY = footTop - all_top.outerHeight();
                    var y = $(this).scrollTop();
                    var isIE = !!window.ActiveXObject;
                    var isIE6 = isIE && !window.XMLHttpRequest;
                    var yy = y;
                    var minY = haederH + ah;

                    if ($(".banner-spread").css("display") == "block") {
                        ah = $(".banner-spread").height();
                    } else {
                        ah = 0;
                    }

                    if (isIE6) {
                        if ($(window).scrollTop() > 500) {
                            all_top.css({"display": "block"})
                        } else {
                            all_top.css({"display": "none"})
                        }
                    } else {
                        if (y > bb) {
                            if (y < maxY) {
                                all_top.css({
                                    "position": "fixed",
                                    "top": "40px",
                                    "bottom": ""
                                })
                            } else {
                                all_top.css({
                                    "position": "absolute",
                                    "top": Hfooter.offset().top - all_top.height() + "px",
                                    "bottom": ""
                                })
                            }
                        } else {
                            all_top.css({
                                "position": "absolute",
                                "top": bb + "px",
                                "bottom": ""
                            })
                        }

                        if (yy >= minY) {
                            nav_top.css({
                                "position": "fixed",
                                "top": "0",
                                "background-color": "transparent"
                            });
                            nav_top.addClass("nav_bg");
                            $(".header").height(haederH + nav_topH)
                        } else {
                            nav_top.css({
                                "position": "relative",
                                "top": "",
                                "background-color": "#ff5b7d"
                            });
                            $(".header").height(haederH);
                            if (nav_top.hasClass("nav_bg")) {
                                nav_top.removeClass("nav_bg")
                            }
                        }
                    }
                }

                $(window).scroll(function () {
                    var scroH = $(this).scrollTop();

                    if (navigator.userAgent.indexOf("MSIE 6.0") > 0 || navigator.userAgent.indexOf("MSIE 7.0") > 0 || navigator.userAgent.indexOf("MSIE 8.0") > 0) {
                        setTimeout(function () {
                            var initTop = $(window).scrollTop();
                            if (initTop == scroH) {
                                scrollEvent(scroH);
                            }
                        }, 1000);
                    } else {
                        scrollEvent(scroH);
                    }
                });
            },
            set_cur: function (topLink, n) {
                if (topLink.hasClass("cur")) {
                    topLink.removeClass("cur")
                }
                $(n).addClass("cur").siblings().removeClass("cur");
            }
        });
        return nav;
    }
)